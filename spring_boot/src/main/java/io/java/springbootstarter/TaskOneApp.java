package io.java.springbootstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskOneApp {

	public static void main(String[] args) {
		SpringApplication.run(TaskOneApp.class, args);
	}

}
